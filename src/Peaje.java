
public class Peaje{

    //ATRIBUTOS
    private String[] filaCoches;
    private String[] flyPass;
    private boolean estadoPeaje;
    private int cantidadCochesAtendidos;
    private int cocheEnAtencion;

    public Peaje(String[] filaCoches){
        this.filaCoches = filaCoches;
        estadoPeaje = true;
        cocheEnAtencion = 0;
        cantidadCochesAtendidos = 1;
        flyPass = new String[ filaCoches.length ];
        //Inicializarlo con espacio en blanco " "
        for(int i = 0; i < flyPass.length; i++){
            flyPass[i] = " ";
        }
    }

    //CONSULTORES

    public String[] getFilaCoches() {
        return filaCoches;
    }

    public String[] getFlyPass() {
        return flyPass;
    }

    public boolean isEstadoPeaje() {
        return estadoPeaje;
    }

    public int getCantidadCochesAtendidos() {
        return cantidadCochesAtendidos;
    }

    public int getCocheEnAtencion() {
        return cocheEnAtencion;
    }

    //MODIFICADORES

    public void setFilaCoches(String[] filaCoches) {
        this.filaCoches = filaCoches;
    }

    public void setFlyPass(String[] flyPass) {
        this.flyPass = flyPass;
    }

    public void setEstadoPeaje(boolean estadoPeaje) {
        this.estadoPeaje = estadoPeaje;
    }

    public void setCantidadCochesAtendidos(int cantidadCochesAtendidos) {
        this.cantidadCochesAtendidos = cantidadCochesAtendidos;
    }

    public void setCocheEnAtencion(int cocheEnAtencion) {
        this.cocheEnAtencion = cocheEnAtencion;
    }

    //ACCIONES
    public void proximoCoche(){
        if(estadoPeaje){
            /* if(cocheEnAtencion < filaCoches.length-1){
                ++cocheEnAtencion; 
                ++cantidadCochesAtendidos;  
            }else{
                estadoPeaje = false;
            }*/
            ++cocheEnAtencion; 
            ++cantidadCochesAtendidos;
            
        }
    }

    public void agregarCocheFlyPass(){
        
    }

    public void cambiarEstadoPeaje(){
        estadoPeaje = !estadoPeaje;
    }

}